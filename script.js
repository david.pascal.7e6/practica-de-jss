class Excel{
    constructor(content){
        this.content = content
    }

    header(){
        return this.content[0]
    }
    
    rows(){
        return new RowCollection(this.content.slice(1, this.content.length))
    }
}

class RowCollection{
    constructor(rows){
        this.rows = rows
    }

    first(){
        return new Row(this.rows[0])
    }

    get(index){
        return new Row(this.rows[index])
    }

    count(){
        return this.rows.length
    }
}

class Row{
    constructor(row){
        this.row = row
    }
    study(){
        return this.row[0]
    }
    sex(){
        return this.row[1]
    }
    studyDate(){
        return this.row[2]
    }
    birthDate(){
        return this.row[3]
    }
    ctId(){
        return this.row[4]
    }
    T_SerieUi(){
        return this.row[5]
    }
}

class ExcelPrinter{
    static print(tableId, excel){
        const table = document.getElementById(tableId)

        excel.header().forEach(title =>{
            table.querySelector("thead>tr").innerHTML += `<td>${title}</td>`
        })

        for(let index=0; index < excel.rows().count(); index++){
            const row= excel.rows().get(index);

            table.querySelector('tbody').innerHTML += `
            <tr>
                <td>${row.study()}</td>
                <td>${row.sex()}</td>
                <td>${row.studyDate()}</td>
                <td>${row.birthDate()}</td>
                <td>${row.ctId()}</td>
                <td>${row.T_SerieUi()}</td>
            </tr>
            `
        }
    }
}
const excelInput= document.getElementById('excel-input')

excelInput.addEventListener('change',async function(){
    const content = await readXlsxFile(excelInput.files[0])

    const excel = new Excel(content)

    console.log(ExcelPrinter.print('excel-table', excel))
}) 